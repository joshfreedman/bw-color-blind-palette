# B.W. Color-Blind Palette

A 7 color palette for the color-blind described by prof. Bang Wong.

Source: Wong, Bang. (2011). Points of view: Color blindness. Nature methods. 8. 441. 10.1038/nmeth.1618..

DOI: [10.1038/nmeth.1618](http://dx.doi.org/10.1038/nmeth.1618).

## Installation

```shell
npm install --save bw-color-blind-palette
```

## Usage

```sass
@import '~bw-color-blind-palette'

$green:     map-get( $bw-color-blind-palette, 'bluish-green' )

.your-class

    color:  $green
```

### Warning on name strings

Sass/SCSS recognizes color names and replaces them with their shortest values.

So this:
```sass
map-get( $bw-color-blind-palette, black )
```
may actually work like this and return null:
```sass
map-get( $bw-color-blind-palette, '#000' )
```

## Colors

| Name | Hex | RGB |
|------|-----|-----|
| `'black'`             | `#000000` | 0, 0, 0 |
| `'orange'`            | `#e69f00` | 230, 159, 0 |
| `'sky-blue'`          | `#56b4e9` | 86, 180, 233 |
| `'bluish-green'`      | `#009e73` | 0, 158, 115 |
| `'yellow'`            | `#f0e442` | 240, 228, 66 |
| `'blue'`              | `#0072b2` | 0, 114, 178 |
| `'vermillion'`        | `#d55e00` | 213, 94, 0 |
| `'reddish-purple'`    | `#ff79a7` | 204, 121, 167 |

##  License

Bw-color-blind-palette is [Apache-2.0 licensed](https://gitlab.com/tokenmill/npm/bw-color-blind-palette/blob/master/LICENSE).
